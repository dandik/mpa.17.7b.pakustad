import React,{Component} from 'react'
import {View, Text, Image, Dimensions, StyleSheet} from 'react-native'

const {heigh,width} = Dimensions.get('window')
const styles = StyleSheet.create({
	wrapperOvo:{
	marginHorizontal:18,
	height:70,
	marginTop: 15,
	backgroundColor:'white',
	elevation: 4,
	borderRadius:10
},
	imageIcon:{
		flexDirection:'row',
		justifyContent:'space-between'
	},
	payOvo1:{		
		flexDirection:'row',
		justifyContent:'space-between',
		height:80,
		marginTop: 25,
		
	},
	
	payOvo2:{		
		flexDirection:'row',
		justifyContent:'space-between',
		height:80,
		marginTop:20,
		
	
	},
	footerOvo:{
	elevation: 8,
	marginLeft:-18.5,
	height:50,
	marginTop:-50,
	backgroundColor:'white',
	width:width,
	flexDirection:'row',
	justifyContent:'space-between',
},
	promoOvo:{
	marginLeft:-18.5,
	height:25,
	marginTop:5,
	width:width,
	flexDirection:'row',
	justifyContent:'space-between',
},
	
	garisOvo:{
		marginLeft:-20,
		width:400,
		marginTop:10,
	borderBottomWidth:3,
	borderColor:'grey'
},
	
})
class Home extends Component{
  render() {
    return (
		<View>
			<Image style={{
							height: 140,
							width:width,
							borderBottomRightRadius:25,
							borderBottomLeftRadius:25 }}
							source={require('./img/ungu.png')} />
			
			
			<Text style={{
							fontSize:25,
							fontWeight:'bold',
							marginTop:-145,
							alignSelf:'center',
							color:'white'}}>OVO</Text>
			<Text style={{
							color:'white',
							marginLeft:20,
							fontSize:12}}>OVO Cash</Text>
			<Text style={{
							color:'white',
							marginLeft:20,
							fontWeight:'bold'
							}}>Rp</Text>
			<Text style={{
							color:'white',
							marginLeft:40,
							marginTop:-10,
							fontSize:15,
							fontWeight:'bold'}}>10.000</Text>
			<Text style={{
							color:'white',
							marginLeft:20,
							fontSize:12}}>OVO Points 0</Text>
			
		<View style={styles.wrapperOvo}>
				<View style={styles.imageIcon}>
				<Image style={{
							marginLeft:50,
							marginTop:10,
							height: 35,
							width:35}}
							source={require('./img/TopUp.png')}/>
				<Text style={{marginTop:45,marginLeft:-90,fontSize:10}}>Top Up</Text>
				<Image style={{
							marginTop:10,
							height: 35,
							width:35}}
							source={require('./img/transfer.png')} />
				<Text style={{marginTop:45,marginLeft:-90,fontSize:10}}>Transfer</Text>
				<Image style={{
							marginRight:-90,
							marginTop:10,
							height: 35,
							width:35}}
							source={require('./img/histori.png')} />
				<Text style={{fontSize:10,marginTop:45,marginRight:50}}>History</Text>
				</View>
				<View style={styles.payOvo1}>
				
				<Image style={{marginLeft:5,height:50,width:50}} source={require('./img/listrik.png')}></Image>
				<Text style={{marginTop:60,marginLeft:-55}}>PLN</Text>
				<Image style={{marginLeft:25,height:50,width:50}}
							source={require('./img/pulsa.png')}/>
				<Text style={{marginTop:60,marginLeft:-60}}>Pulsa</Text>
				<Image style={{marginLeft:25,height:50,width:50}}
							
							source={require('./img/kuota.png')}/>
				<Text style={{marginTop:60,marginLeft:-75}}>Paket Data</Text>
				<Image style={{marginRight:-65,height:50,width:50}}
							
							source={require('./img/pdam.png')}/>
				<Text style={{marginTop:60,marginRight:15}} >PDAM</Text>
			
				</View>
				
				<View style={styles.payOvo2}>
				
				<Image style={{marginLeft:5,height:50,width:50}} source={require('./img/bpjs.png')}></Image>
				<Text style={{marginTop:60,marginLeft:-65}}>BPJS</Text>
				<Image style={{marginLeft:10,height:50,width:50}}
							source={require('./img/Tv.png')}/>
				<Text style={{marginTop:60,marginLeft:-70}}>TV Kabel</Text>
				<Image style={{marginLeft:10,height:50,width:50}}
							
							source={require('./img/telpon.png')}/>
				<Text style={{marginTop:60,marginLeft:-70}}>Telpon</Text>
				<Image style={{marginRight:-80,height:50,width:50}}
							
							source={require('./img/lain.png')}/>
				<Text style={{marginTop:60,marginRight:15}} >Lainnya</Text>
			
				</View>
				<View style={styles.garisOvo}></View>
				<View style={styles.promoOvo}>
					<Text style={{fontWeight:'bold',marginLeft:10}}>Info dan Promo Spesial</Text>
					<Text style={{fontWeight:'bold',marginRight:10, color:'aquamarine'}}>Lihat Semua</Text>
						</View>
				<Image style={{
						height:150,
						marginHorizontal:0,
						width:325,
						marginTop:5,
						borderRadius:10,
				}}source={require('./img/NEWSLETTER-600x300.jpg')} />
	
				<View style={styles.footerOvo}>
				<Image style={{
							marginLeft:20,
							marginTop:5,
							height: 30,
							width:30}}
							source={require('./img/icons8-home-page-64.png')}/>
				<Text style={{marginTop:33,marginLeft:-75,fontSize:10}}>Home</Text>
				<Image style={{
							marginLeft:-10,
							marginTop:5,
							height: 30,
							width:30}}
							source={require('./img/icons8-online-shop-sale-64.png')} />
				<Text style={{marginTop:33,marginLeft:-73,fontSize:10}}>Deals</Text>
				<Image style={{
							borderRadius:10,
							marginRight:-100,
							marginTop:-20,
							height: 40,
							width:40}}
							source={require('./img/tengah.png')} />
				<Text style={{fontSize:10,marginTop:33,marginLeft:25}}>Scan</Text>
				<Image style={{
							marginLeft:-10,
							marginTop:5,
							height: 35,
							width:35}}
							source={require('./img/icons8-average-2-64.png')}/>
				<Text style={{marginTop:33,marginLeft:-75,fontSize:10}}>Finance</Text>
				<Image style={{
						marginRight:-75,
							marginTop:3,
							height: 35,
							width:35}}
							source={require('./img/icons8-customer-64.png')} />
				<Text style={{marginTop:33,fontSize:10,marginRight:20}}>Profile</Text>

				</View>	
			
		</View>
		</View>
		
		
            )
			
  }
}
export default Home