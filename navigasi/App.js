import 'react-native-gesture-handler';
import React, {Component} from 'react' ;
import Router from './src/config/router';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {Platform, StyleSheet, Text, View, Image, ScrollView} from 'react-native';

import Header from './src/molecules/Header';
import Ovopay from './src/molecules/Ovopay';
import Layanan from './src/molecules/Layanan';
import Garis1  from './src/molecules/Garis1';
import Conten  from './src/molecules/Conten';
import Footer  from './src/molecules/Footer';

class App extends Component {
  render() {
    return (
		<Router />      
    );
  }
}


export default App;