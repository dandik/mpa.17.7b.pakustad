import Home from './Home';
import Topup from './Topup';
import Transfer from './Transfer';
import History from './History';
import PLN from './PLN'
import Telkom from './Telkom'
import PDAM from './PDAM'
import BPJS from './BPJS'
import Pulsa from './Pulsa'

export {

	Home,
	Topup,
	Transfer,
	History,
	PLN,
	Telkom,
	PDAM,
	BPJS,
	Pulsa

}