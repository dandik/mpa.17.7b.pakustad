import 'react-native-gesture-handler';
import React, {Component} from 'react' ;
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import {Platform, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity} from 'react-native' 

import Header from '../../../molecules/Header';
import Ovopay from '../../../molecules/Ovopay';
import Layanan from '../../../molecules/Layanan';
import Garis1  from '../../../molecules/Garis1';
import Conten  from '../../../molecules/Conten';
import Footer  from '../../../molecules/Footer';
import Topup from '../Topup'
class Home extends Component {
  render() {
    return (
   			    <View style={{flex:1}}>
		<ScrollView style={{flex:1, backgroundColor:'white'}}>
			
			<Header />
	 		<Ovopay onPress={() => this.props.navigation.navigate('Topup')}/>
			<Layanan />
			<Garis1 />
			<Conten />
			
   		</ScrollView>
		
   			<Footer />

      </View>
    );
  }
}

export default Home;