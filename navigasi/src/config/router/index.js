import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Home, Topup, Transfer, History, PLN, Telkom, PDAM, BPJS,Pulsa} from '../../containers/pages';

const  Router = createStackNavigator(

	{
		
 		 Home: {	
  		  screen: Home,
  		},

  		Topup: {	
  		  screen: Topup
  		},
  		Transfer: {	
  		  screen: Transfer
  		},

      History: { 
        screen: History
      },

       PLN: { 
        screen:PLN
      },

       PDAM: { 
        screen:PDAM
      },

        Telkom: { 
        screen:Telkom
      },

        BPJS: { 
        screen:BPJS
      },

        Pulsa: { 
        screen:Pulsa
      },
	},
	{ 
		headerMode:'none',
		initialRouteName:'Pulsa'
	}

);

export default createAppContainer(Router);