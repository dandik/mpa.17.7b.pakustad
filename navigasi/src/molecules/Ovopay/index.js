import 'react-native-gesture-handler';
import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';     

const Ovopay = ({onPress}) => {
	return (

			<View style={{flexDirection:'row',marginHorizontal:18,height:50,marginTop: 15,backgroundColor:'white',elevation: 4,borderRadius:10}}>
				<TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center'}}onPress={() => Ovopay.props.navigation.navigate('Topup')}>
					<Image style={{width:23, height:23, marginTop:5}}source={require('../../icon/topup.png')}/>
					<Text style={{fontSize:13, color:'grey'}}>Top Up</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center'}}onPress={() => Ovopay.props.navigation.navigate('Transfer')}>
					<Image style={{width:23, height:23, marginTop:5}}source={require('../../icon/transfer.png')}/>
					<Text style={{fontSize:13, color:'grey'}}>Transfer</Text>
				</TouchableOpacity>
				<TouchableOpacity style={{flex:1, alignItems:'center', justifyContent:'center'}}onPress={() => Ovopay.props.navigation.navigate('History')}>
					<Image style={{width:23, height:23, marginTop:5}}source={require('../../icon/histori.png')}/>
					<Text style={{fontSize:13, color:'grey'}}>Histori</Text>
				</TouchableOpacity>
			</View>
		)
}
     

export default Ovopay;



