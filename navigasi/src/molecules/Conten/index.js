
import React from 'react';
import { View, Text, Image } from 'react-native';     

const Header = () => {
	return (
			
			<View>
				<View style={{flexDirection:'row',marginTop:10}}>
					<Text style={{marginLeft:15,fontSize:15, color:'black',fontWeight:'bold'}}>Info dan Promo Spesial</Text>
					<Text style={{marginLeft:100,marginTop:4,fontSize:12, color:'#66CDAA',fontWeight:'bold'}}>Lihat Semua</Text>
				</View>

				<View style={{marginHorizontal:18,marginTop: 15}}>
					<Image style={{height:125, width:'100%',borderRadius:10}}source={require('../../icon/conten1.png')}/>
				</View>
			</View>
		)
}
     

export default Header;