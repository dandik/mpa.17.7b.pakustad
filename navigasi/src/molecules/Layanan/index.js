import React from 'react';
import { View, Text, Image } from 'react-native';     

const Layanan = () => {
	return (

			
			<View style={{flexDirection:'row',flexWrap:'wrap',marginHorizontal:18, marginTop:16}}>
				<View style={{justifyContent:'space-between',flexDirection:'row',width:'100%',marginBottom:20}}>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/listrik.png')}/>
						<Text style={{fontSize:13, color:'black'}}>PLN</Text>
					</View>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/pulsa.png')}/>
						<Text style={{fontSize:13, color:'black'}}>Pulsa</Text>
					</View>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/game.png')}/>
						<Text style={{fontSize:13, color:'black'}}>Game</Text>
					</View>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/pdam.png')}/>
						<Text style={{fontSize:13, color:'black'}}>PDAM</Text>
					</View>
				</View>

				<View style={{justifyContent:'space-between',flexDirection:'row',width:'100%',marginButtom:18}}>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/bpjs.png')}/>
						<Text style={{fontSize:13, color:'black'}}>BPJS</Text>
					</View>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/internet.png')}/>
						<Text style={{fontSize:13, color:'black'}}>TV Kabel</Text>
					</View>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/proteksi.png')}/>
						<Text style={{fontSize:13, color:'black'}}>Proteksi</Text>
					</View>
					<View style={{alignItems:'center', justifyContent:'center'}} >
						<Image style={{width:40, height:40}}source={require('../../icon/lainnya.png')}/>
						<Text style={{fontSize:13, color:'black'}}>Lainnya</Text>
					</View>
				</View>
			</View>
				
		)
}
     

export default Layanan;